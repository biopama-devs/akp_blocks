<?php

/**
 * @file
 */
namespace Drupal\akp_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;

/**
 * Creates a AKP Footer Block
 * @Block(
 * id = "block_akp_footer",
 * admin_label = @Translation("AKP Footer block"),
 * category = @Translation("AKP"),
 * )
 */
class AkpFooterBlock extends BlockBase implements BlockPluginInterface{

    /**
     * {@inheritdoc}
     */
    public function build() {
        return array (
			'#theme' => 'footer',
        );
    }

}