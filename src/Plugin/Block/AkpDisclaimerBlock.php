<?php

/**
 * @file
 */
namespace Drupal\akp_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;

/**
 * Creates a AKP Disclaimer Block
 * @Block(
 * id = "block_akp_disclaimer",
 * admin_label = @Translation("AKP Disclaimer block"),
 * category = @Translation("AKP"),
 * )
 */
class AkpDisclaimerBlock extends BlockBase implements BlockPluginInterface{

    /**
     * {@inheritdoc}
     */
    public function build() {
        return array (
			'#theme' => 'disclaimer',
        );
    }

}