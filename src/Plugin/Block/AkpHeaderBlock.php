<?php

/**
 * @file
 */
namespace Drupal\akp_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;

/**
 * Creates a AKP Header Block
 * @Block(
 * id = "block_akp_header",
 * admin_label = @Translation("AKP Header block"),
 * category = @Translation("AKP"),
 * )
 */
class AkpHeaderBlock extends BlockBase implements BlockPluginInterface{

    /**
     * {@inheritdoc}
     */
    public function build() {
        return array (
			'#theme' => 'header',
        );
    }

}