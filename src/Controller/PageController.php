<?php
namespace Drupal\akp_blocks\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the akp_blocks module.
 */
class PageController extends ControllerBase {
  public function homepage() {
    $element = array(
	  '#theme' => 'homepage',
    );
    return $element;
  }

  public function partners_team() {
    $element = array(
	  '#theme' => 'partners_team',
    );
    return $element;
  }

  public function about_us() {
    $element = array(
	  '#theme' => 'about_us',
    );
    return $element;
  }

  public function arcx() {
    $element = array(
	  '#theme' => 'arcx',
    );
    return $element;
  }

  public function accessibility() {
    $element = array(
	  '#theme' => 'accessibility',
    '#attached' => [
        'library' => [
          'akp_blocks/accessibility'
        ],
      ],
    );
    return $element;
  }
}